/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author danim
 */
public class Time {
    private HashMap<String, Jogador> jogadores = new HashMap<>();
    
    public HashMap<String, Jogador> getJogadores(){
        return jogadores;
    }
    
    public void addJogador(String posicao, Jogador j){
        jogadores.put(posicao, j);
    }
    
    public List<Jogador> ordena(JogadorComparator a){
        List<Jogador> retorno = new ArrayList<>(jogadores.values());
        Collections.sort(retorno, a);
        return retorno;
    }
}