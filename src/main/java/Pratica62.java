/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
*/

import java.util.Collections;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;
import utfpr.ct.dainf.if62c.pratica.Time;


public class Pratica62 {
    public static void main(String[] args) {
    Time time1 = new Time();
    JogadorComparator a = new JogadorComparator(true, true, false);
    Jogador b = new Jogador(1, "Eu");
    
    time1.addJogador("Zagueiro", b);
    time1.addJogador("Goleiro", new Jogador(1, "Fulano"));
    time1.addJogador("Lateral", new Jogador(4, "Ciclano"));
    time1.addJogador("Atacante", new Jogador(10, "Beltrano"));
    
    System.out.println("Posição\t\t\t\tTime 1\t\t\t\tTime 2");
        
            System.out.println(time1.ordena(a));
        
            System.out.println(Collections.binarySearch(time1.ordena(a), b));
    }
}